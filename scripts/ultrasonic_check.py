''' Parallax Ping))) 
	Range = 2 cm to 3 m
	trigger pulse = 2 us, typical 5 us
	Echo Return Pulse Minimum = 115 us
	Echo Return Pulse Maximum = 18.5 ms
	Delay before nextmeasurement = 200 us
'''

import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

def startTimer():
	start_timer = (time.time())
	return start_timer

def triggerUltraSonicAndGetDistance():
	GPIO.setup(24,GPIO.OUT)
	GPIO.output(24, 0)
	time.sleep(0.000002)

	GPIO.output(24,1)
	time.sleep(0.000005)

	GPIO.output(24, 0)
	GPIO.setup(24,GPIO.IN)

	while 0 == GPIO.input(24):
		
		continue

	start_timer = startTimer()
	print("start timr ", start_timer)

	while 1 == GPIO.input(24):
		continue

	distance_in_cms = stopTimerAndCalculateDistance(start_timer)
	return distance_in_cms

def stopTimerAndCalculateDistance(start_timer):
	stop_timer = (time.time())
	print("stop timr ", stop_timer)
	calculated_distance_in_cms = (stop_timer - start_timer) * 34000 / 2
	return calculated_distance_in_cms


'''while 1:
	distance_in_inches = triggerUltraSonicAndGetDistance()
	print(distance_in_inches)
	time.sleep(1)'''

