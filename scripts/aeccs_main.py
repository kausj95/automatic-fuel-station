#!/usr/bin/env_python

'''Common (required) hexadecimal to decimal conversions
	x55 = 85
	x3E8 = 1000 (xE8 = 232)
	x5AA = 1450 (xAA = 170)
	x5DC = 1500 (xDC = 220)
	x7D0 = 2000 (xD0 = 208)
	x9A6 = 2470 (xA6 = 166)
	x9C4 = 2500 (xC4 = 196)
	x226 =  550 (x26 =  38)
	x94C = 2380 (x4C =  76)
	x8FC = 2300 (xFC = 252)
	x898 = 2200 (x98 = 152)
   
   Command Format
	| 85 | 85 | no of | cmd no | no of servos |    time    |    time    | servo id | angle pos  | angle pos  |
	|    | 	  | bytes |	   |  to control  | lower byte | upper byte |          | lower byte | upper byte |

   Initial servo positions
   	ID1 = 2200
   	ID2 = 1450
   	ID3 =  600
   	ID4 =  900
   	ID5 = 1000
   	ID6 =  600

   After detecting car
	ID1 = 2200
   	ID2 = 1450
   	ID3 =  600
   	ID4 = 1450
   	ID5 = 1450
   	ID6 = 1500
'''

'''
pixel width = 2.688mm
'''
import datetime
import os
import RPi.GPIO as GPIO
import time
import re

import time
import serial 
import def_serial as Uart
import servo_init_pos
import servo_controller as svc
import ultrasonic_check as us
import math

from picamera import PiCamera

camera = PiCamera()

start_timer = 0
stop_timer = 0

def captureImage():
    print("Capturing image\n")
    camera.start_preview()
    time.sleep(2)
    img = camera.capture('/home/pi/Desktop/newDarknet/darknet/image.jpg')
    camera.stop_preview()

def detectObject():
    completeFlag = 0
    if(os.path.isfile("/home/pi/Desktop/newDarknet/darknet/predictions.txt")):
        os.remove("/home/pi/Desktop/newDarknet/darknet/predictions.txt")
    os.system('./darknet detector test /home/pi/Desktop/newDarknet/darknet/cfg/power-socket-tiny.data /home/pi/Desktop/newDarknet/darknet/cfg/power-socket-tiny.cfg /home/pi/Desktop/newDarknet/darknet/weights/yolov3-tiny_last.weights /home/pi/Desktop/newDarknet/darknet/image.jpg -thresh 0.4 >> /home/pi/Desktop/newDarknet/darknet/predictions.txt')
    completeFlag = 1
    #print("port detected\n")
    return completeFlag

def getObjectCoordinates():
    pattern = re.compile("\w+.(\d+).\s\w+.(\d+).\s\w+.(\d+).\s\w+.(\d+)")
    pattern_socket_percentage = re.compile("\w+.\s(\d+).")

    F = open("/home/pi/Desktop/newDarknet/darknet/predictions.txt")
    
    left, right, top, bottom = 0, 0, 0, 0    
    high, count, lineNum = 0, 0, 0
    
    for line in F:    
        for match in re.finditer(pattern_socket_percentage,line):
            if(match.group(1) > high):
                high = match.group(1)
                lineNum = count
            count+=1
            
        count = 0
        
        for match in re.finditer(pattern,line):
            #print(match)
            
            if(lineNum == count):
                left = int(match.group(1))
                top = int(match.group(2))
                right = int(match.group(3))
                bottom = int(match.group(4))
            else:
                count+=1
                
    print("high ", high)
    print(left,", ",top,", ",right,", ",bottom,"\n")
            #checkCondition()
    F.close()
    
    

    return left,top,right,bottom
        
def calculateMidAndReturnMovement(left,top,right,bottom, distance_in_cms):
    midImage_X = 2592/2
    midImage_Y = 1944/2

    xPixel = 2592
    yPixel = 1944

    vertDegreeAngle = 1944 / 41
    horizDegreeAngle = 2592 / 54

    '''
    diagonalAngleView = math.sqrt((vertDegreeAngle*vertDegreeAngle)+(horizDegreeAngle*horizDegreeAngle))
    diagonalPixelView = math.sqrt((xPixel*xPixel)+(yPixel*yPixel))
    
    anglePerPixel = diagonalAngleView/diagonalPixelView
    '''

    anglePerPixel = 41 / 1944

    midObject_X = (left+right)/2
    midObject_Y = (top+bottom)/2

    pixel_X = midImage_X - midObject_X
    pixel_Y = midImage_Y - midObject_Y

    print("pixel y =",pixel_Y)
    #print("pixel y cha tan =",math.tan(abs(pixel_Y)))
    print("chand theta = ", ((midObject_Y - 972)*41)/1944.0)
    #print("theta = ",abs(pixel_Y)*anglePerPixel)
    #print("pixel y cha tan*anglePerPixel =",math.tan(abs(pixel_Y)*anglePerPixel))
    
    #tan_theta = math.tan(abs(pixel_Y)*anglePerPixel)
    tan_theta = math.tan(math.radians(abs(((midObject_Y - 972)*41)/1944.0)))

    width_px, height_px = [], []

    distance_in_cms = distance_in_cms - 7	#3 + 4 -> depth of port + distance between camera and ultrasonic
    if(pixel_X < 0 and pixel_Y < 0):
        width_px.extend([0,pixel_X])
        height_px.extend([0,(tan_theta*distance_in_cms)])

    elif(pixel_X < 0 and pixel_Y > 0):
        width_px.extend([0,pixel_X])
        height_px.extend([1,(tan_theta*distance_in_cms)])

    elif(pixel_X > 0 and pixel_Y < 0):
        width_px.extend([1,pixel_X])
        height_px.extend([0,(tan_theta*distance_in_cms)])

    elif(pixel_X > 0 and pixel_Y > 0):
        width_px.extend([1,pixel_X])
        height_px.extend([1,(tan_theta*distance_in_cms)])

    print(width_px, height_px)

    return [width_px, height_px]
 
if (__name__ == "__main__"):
	while(1):
		time.sleep(1)	    
		while(1):
			#distance = triggerUltraSonicAndGetDistance()

			#while(distance > 25):
			    #print("Car Detected - Signal Camera")
			servo_init_pos.init_position()
			time.sleep(1)
			distance_in_cms = us.triggerUltraSonicAndGetDistance()
			print(distance_in_cms)
			ht_reqd = 0
			dist = 0
			if distance_in_cms <= 30 and distance_in_cms >= 25:
				svc.img_capture_pos()
				time.sleep(3)

				captureImage()
				if(detectObject()):
					left,top,right,bottom = getObjectCoordinates()
				  	if (left == right == right == bottom):
						print("port not detected")
						break
					movement = calculateMidAndReturnMovement(left,top,right,bottom, int(distance_in_cms))
			    	time.sleep(4)
				#ht_dif_camera_plug = pixel_calc
				ht_reqd = 19.5 - movement[1][1]
				#time.sleep(1)
				ser_4, ser_5, h_dist_trvld = svc.align_arm_to_port(ht_reqd)#(ht_reqd)		#height of port from motor 5 base
				time.sleep(2)
				print("h_dist_travelled",h_dist_trvld)
				dist = distance_in_cms - 3 #21		#dist of port from arm base
				print("distance = ", dist)
				svc.insert_charger_into_port(ser_4, ser_5, h_dist_trvld, dist)
				time.sleep(5)
				servo_init_pos.init_position()

