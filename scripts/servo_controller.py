#!/usr/bin/env_python

'''Common (required) hexadecimal to decimal conversions
	x55 = 85
	x3E8 = 1000 (xE8 = 232)
	x5AA = 1450 (xAA = 170)
	x5DC = 1500 (xDC = 220)
	x7D0 = 2000 (xD0 = 208)
	x9A6 = 2470 (xA6 = 166)
	x9C4 = 2500 (xC4 = 196)
	x226 =  550 (x26 =  38)
	x94C = 2380 (x4C =  76)
	x8FC = 2300 (xFC = 252)
	x898 = 2200 (x98 = 152)
   
   Command Format
	| 85 | 85 | no of | cmd no | no of servos |    time    |    time    | servo id | angle pos  | angle pos  |
	|    | 	  | bytes |	   |  to control  | lower byte | upper byte |          | lower byte | upper byte |

   Initial servo positions
   	ID1 = 2200
   	ID2 = 1450
   	ID3 =  600
   	ID4 =  900
   	ID5 = 1000
   	ID6 =  600

   After detecting car
	ID1 = 2200
   	ID2 = 1450
   	ID3 =  600
   	ID4 = 1450
   	ID5 = 1450
   	ID6 = 1500
'''

###### TIP: If val <= 255 use the value directly; however, if val > 255, right shift (>>) by 8 to get upper byte 
######      and "&" the original value with 255 to get lower byte (min = 500 and max = 2500)


import time
import serial 
import def_serial as Uart
import servo_init_pos
import math

#servo_init_pos.init_position()

servo_four_arm_len = 9.0
claw_len = 10

#servo_cmd = [85, 85, 14, 3, 3, 232, 3, 3, 38, 2, 4, 170, 5, 5, 170 ,5]

def img_capture_pos():
	servo_cmd_img_capture = [85, 85, 17, 3, 4, 232, 3, 6, 220, 5, 5, 170, 5, 4, 170, 5, 3, 38, 2]
	Uart.ser.write(bytearray(servo_cmd_img_capture))
	time.sleep(1)

def align_arm_to_port(ht):
	servo_5_val, servo_4_val = 0, 0
	servo_cmd = [85, 85, 14, 3, 3, 232, 3, 3, 38, 2, 4, 0, 0, 5, 0 ,0]
	req_ht = float(ht) - (servo_four_arm_len) ##REVIEW #- 2.5)	#2.5 -> height upto camera mount
	print("req_ht",req_ht)
	req_angle_rad = math.acos(req_ht / 10.5)	#servo-5 arm length  = 10.5 cm
	h_dist = (math.sin(req_angle_rad)) * 10.5
	req_angle_deg = math.degrees(req_angle_rad)
	req_servo_pulse = 10.67 * req_angle_deg		#10.67 us pulse reqd to move servo by 1 degree
	servo_5_val = int (1450 + req_servo_pulse)		#arm perpendicular to ground, pulse = 1450 and then adding cal value to forward the arm
	servo_4_val = int (1450 + req_servo_pulse)		#arm perpendicular to ground, pulse = 1450 and then adding cal value to move the arm backward
	servo_cmd[11] = servo_4_val & 255
	servo_cmd[12] = (servo_4_val >> 8) & 255
	servo_cmd[14] = servo_5_val & 255
	servo_cmd[15] = (servo_5_val >> 8) & 255
	Uart.ser.write(bytearray(servo_cmd))
	time.sleep(1)
	print("h_dist",h_dist)
	return servo_4_val, servo_5_val, h_dist

def insert_charger_into_port(ser_4, ser_5, h_dist_trvld, dist):
	servo_cmd = [85, 85, 14, 3, 3, 232, 3, 3, 38, 2, 4, 0, 0, 5, 0 ,0]
	req_dist = round(dist - h_dist_trvld, 2)
	print("req_dist",req_dist)
	print(math.asin((req_dist - 7.5) / 19.5))		#10 + 9.5
	req_angle_rad = math.asin((req_dist-7) / 19.5)
	req_angle_deg = math.degrees(req_angle_rad)
	print("req_angle_deg",req_angle_deg)	
	req_servo_pulse = 10.67 * req_angle_deg
	print("servo_pulses insert plug",req_servo_pulse)
	ser_3_val = int (550 + req_servo_pulse)
	servo_cmd[8] = ser_3_val & 255
	servo_cmd[9] = (ser_3_val >> 8) & 255
	ser_4 = int (ser_4 - req_servo_pulse)
	servo_cmd[11] = ser_4 & 255
	servo_cmd[12] = (ser_4 >> 8) & 255
	servo_cmd[14] = ser_5 & 255
	servo_cmd[15] = (ser_5 >> 8) & 255
	Uart.ser.write(bytearray(servo_cmd))
	time.sleep(1)
	


#img_capture_pos()
#time.sleep(1)
#ser_4, ser_5, h_dist_trvld = align_arm_to_port(14)		#height of port from motor 5 base
#time.sleep(1)
#dist = 21		#dist of port from arm base
#insert_charger_into_port(ser_4, ser_5, h_dist_trvld, dist)
#servo_init_pos.init_position()
