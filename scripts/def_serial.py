#!/usr/bin/env_python

### Defining serial port (miniUART) GPIO14 (pin8) as TX and GPIO15 (pin10) as RX
### pins. As servo board is pre-configured with 9600 baudrate, same baudrate is set
### for UART in RPi

import time
import serial

ser = serial.Serial(
	port='/dev/serial0',
	baudrate = 9600,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
	timeout=1
)
