#!/usr/bin/env_python

'''Common (required) hexadecimal to decimal conversions
	x55 = 85
	x3E8 = 1000 (xE8 = 232)
	x5AA = 1450 (xAA = 170)
	x5DC = 1500 (xDC = 220)
	x7D0 = 2000 (xD0 = 208)
	x9A6 = 2470 (xA6 = 166)
	x9C4 = 2500 (xC4 = 196)
	x226 =  550 (x26 =  38)
	x94C = 2380 (x4C =  76)
	x8FC = 2300 (xFC = 252)
	x898 = 2200 (x98 = 152)
   
   Command Format
	| 85 | 85 | no of | cmd no | no of servos |    time    |    time    | servo id | angle pos  | angle pos  |
	|    | 	  | bytes |	   |  to control  | lower byte | upper byte |          | lower byte | upper byte |

   Initial servo positions
   	ID1 = 2200
   	ID2 = 1450
   	ID3 =  600
   	ID4 =  900
   	ID5 = 1000
   	ID6 =  600

   After detecting car
	ID1 = 2200
   	ID2 = 1450
   	ID3 =  600
   	ID4 = 1450
   	ID5 = 1450
   	ID6 = 1500
'''

import def_serial as Uart

def init_position():
	servo_cmd = [85, 85, 8, 3, 1, 232, 3, 1, 152, 8]	#2200
	#servo_cmd = b'\x55\x55\x08\x03\x01\xE8\x03\x01\xD0\x07' #ID:1
	Uart.ser.write(bytearray(servo_cmd))
	Uart.time.sleep(1)

	servo_cmd = [85, 85, 8, 3, 1, 232, 3, 2, 170, 5]	#1450
	#servo_cmd = b'\x55\x55\x08\x03\x01\xE8\x03\x02\xAA\x05' #ID:2
        Uart.ser.write(bytearray(servo_cmd))
        Uart.time.sleep(1)

	servo_cmd = [85, 85, 14, 3, 3, 232, 3, 3, 88, 2, 4, 132, 3, 5, 232, 3]		#600        
	#servo_cmd = b'\x55\x55\x08\x03\x01\xE8\x03\x03\x58\x02' #ID:3
        Uart.ser.write(bytearray(servo_cmd))
        Uart.time.sleep(1)

	servo_cmd = [85, 85, 8, 3, 1, 232, 3, 6, 58, 2]	#600
	#servo_cmd = b'\x55\x55\x08\x03\x01\xE8\x03\x06\xDC\x05' #ID:6
        Uart.ser.write(bytearray(servo_cmd))
        Uart.time.sleep(1)
