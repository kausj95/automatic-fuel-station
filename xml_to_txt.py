import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
import csv

def convert(w,h,x0,x1,y0,y1):
    dw = 1./w
    dh = 1./h
    x = (x0 + x1)/2.0
    y = (y0 + y1)/2.0
    w = x1 - x0
    h = y1 - y0
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (0,x,y,w,h)

def xml_to_df(path):
    xml_list = []
    tree = ET.parse(path)
    root = tree.getroot()
    for member in root.findall('object'):
        value = (convert(int(root.find('size')[0].text), 
                        int(root.find('size')[1].text),
                        int(member[4][0].text),
                        int(member[4][2].text),
                        int(member[4][1].text),
                        int(member[4][3].text)
                ))
        

                        #root.find('filename').text,
                #int(root.find('size')[0].text),
                #int(root.find('size')[1].text),

            #    0,
            #     int(member[4][0].text),
            #     int(member[4][1].text),
            #     int(member[4][2].text),
            #     int(member[4][3].text)
            #     )

        xml_list.append(value)
    column_name = ['class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns = column_name)
    return xml_df


def main():
    entries = os.listdir("C:/Users/kaust/automatic-fuel-station/example/")


    result = pd.DataFrame()

    for i in entries:
        if i.endswith('xml'):
            image_path = os.path.join(os.getcwd(),"example/"+i)
            #print(image_path)
            xml_df = xml_to_df(image_path)
            #print(xml_df)
            xml_df.to_csv('port_labels_'+i+'.csv', index=None, header = False)

            with open(i.replace('xml','txt'), "w") as my_output_file:
                with open('port_labels_'+i+'.csv', "r") as my_input_file:
                    [my_output_file.write(" ".join(row)+'\n') for row in csv.reader(my_input_file)]
                    #print('Successfully converted {} to text.'.format(i))

main()