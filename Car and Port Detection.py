import datetime
import os
import RPi.GPIO as GPIO
import time
import re

from picamera import PiCamera

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
camera = PiCamera()

start_timer = 0
stop_timer = 0


def triggerUltraSonicAndGetDistance():

    GPIO.setup(24,GPIO.OUT)
    GPIO.output(24,GPIO.LOW)
    time.sleep(2)

    GPIO.output(24,GPIO.HIGH)
    time.sleep(5)

    GPIO.output(24,GPIO.LOW)
    GPIO.setup(24,GPIO.IN)

    while(!GPIO.INPUT(24)):
        continue
    
    startTimer()
    
    while(GPIO.INPUT(24)):
        continue
    
    distance_in_inches = stopTimerAndCalculateDistance()

    return distance_in_inches

def startTimer():
    start_timer = int(time.time())

def stopTimerAndCalculateDistance():
    stop_timer = int(time.time())
    calculated_distance_in_inches = (stop_timer - start_timer) / 147
    
    return calculated_distance_in_inches

def captureImage():
    camera.start_preview()
    time.sleep(2)
    img = camera.capture('/home/pi/Desktop/image.jpg')
    camera.stop_preview()

def detectObject():
    os.system('cd ../home/pi/Desktop/darknet')
    os.system('./darknet detector test cfg/power-socket-tiny.data cfg/power- socket-tiny.cfg power-socket-tiny_1200.weights img -thresh 0.01 >> predictions.txt')

def getObjectCoordinates():
    pattern = re.compile("\w+.(\d+).\s\w+.(\d+).\s\w+.(\d+).\s\w+.(\d+)")

    F = open("/home/pi/Desktop/darknet/img.txt")
    
    left, right, top, bottom = 0, 0, 0, 0    
	
    for line in F:
        for match in re.finditer(pattern,line):
            #print(match)
            left = int(match.group(1))
            top = int(match.group(2))
            right = int(match.group(3))
            bottom = int(match.group(4))
            #checkCondition()
    F.close()
    
    os.remove("/home/pi/Desktop/darknet/img.txt")

    return left,top,right,bottom
		
def calculateMid(left,top,right,bottom):
    midImage_X = 1920/2
    midImage_Y = 1080/2

    midObject_X = (left+right)/2
    midObject_Y = (top+bottom)/2

    pixel_X = midImage_X - midObject_X
    pixel_Y = midImage_Y - midObject_Y

    width_px, height_px = [], []

    if(pixel_X < 0 && pixel_Y < 0):
	    width_px.extend([0,pixel_X])
	    height_px.extend([0,pixel_Y])

    else if(pixel_X < 0 && pixel_Y > 0):
	    width_px.extend([0,pixel_X])
	    height_px.extend([1,pixel_Y])

    else if(pixel_X > 0 && pixel_Y < 0):
	    width_px.extend([1,pixel_X])
	    height_px.extend([0,pixel_Y])

    else if(pixel_X > 0 && pixel_Y > 0):
	    width_px.extend([1,pixel_X])
	    height_px.extend([1,pixel_Y])

    return [width_px, height_px]
 
if (__name__ == "__main__"):
    while(1):
        distance = triggerUltraSonicAndGetDistance()

        while(distance > 25):
            #print("Car Detected - Signal Camera")

        captureImage()
        detectObject()
        left,top,right,bottom = getObjectCoordinates()
        calculateMid(left,top,right,bottom)
    

        time.sleep(100)

    
