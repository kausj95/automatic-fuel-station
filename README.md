# Automatic Fuel Station

Autonomous vehicles are one of the most emerging research areas of the decade. According to a survey conducted by ABI research, 8 million level 3 autonomous cars will be running worldwide till 2025. Hence, more intelligent features and smart accessories will come up for betterment of autonomous cars in next few years. These systems will upgrade existing systems and try to reduce human efforts for simple but very essential tasks such as re-fuelling the cars.

Today, each and every car owner has to visit a gas station to refuel or needs to plug in his car charger if the car is electric. If a tiresome day is considered, waiting in gas station queue and afterwards facing regular traffic is frustrating. In such cases, a fully autonomous car getting refueled on its own, will definitely save time. Also, many of the times, people forget to plug in their car charger at nights and do not get charged car in the morning. To tackle this mundane issue, imagine the charging point connects to the car port automatically and charges the car. Hence, Autonomous Fuel Station system, which will automate the fuelling process, simplifies human life and is essential in this age of automations.

Based on this idea, we propose a new prototype system of autonomous charging station using computer vision. In this system, car charging station will use object detection algorithm to detect the car and its charging port. A robotic arm will extend until it reaches the car’s charging port and connect to the port on its own. After receiving battery full signal from the car, the arm is detached from the port and returns to the original position. This visionary system prototype will be useful in future era of autonomous electric cars to reduce human efforts.

#Implementation

To start with the project, get a Raspberry Pi model 3B+.a PiCamera and LeWaNSoul 6DoF Robotic arm. Connect UART pins on Raspberry Pi tx(gpio14) to LSC's Rx and  Raspberry pi rx(gpio15) to LSC's tx. Connect ground and power wires. Place camera on the robotic arm such that it can capture image of wall socket placed in front and you are all set up on the hardware side.

clone this repository to your Raspberry Pi using command: 'git clone https://gitlab.com/kausj95/automatic-fuel-station.git'

The project uses YOLOv3-tiny algorithm for training the Machine Learning model to recognize and detect wall socket.

Hence, Clone YOLO darknet repository using command: 'git clone https://github.com/pjreddie/darknet'

Go into the darknet directory and execute 'make' command.

Copy the files power-socket-tiny.data, power-socket-tiny.names, power-socket-tiny.cfg into ~/darknet/cfg/

Copy the files aeccs.py, power-socket-tiny_train.txt and power-socket-tiny_test.txt into ~/darknet/

execute aeccs.py file using command 'python aeccs.py' keeping wall socket in fromt of the arm on 30 to 40 cms distance.